<?php

class SidelightController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['sidelights'] = Sidelight::all();
		return View::make('products/sidelights/index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['configurations'] = Configuration::all();
		$data['shapes'] = Shape::where('type', '=', 'sidelight')->get();

		$attribtypes = Attribtype::all();
		foreach ($attribtypes as $attribtype) {
			$attributes = Attribute::select(DB::raw('
				id,
				attribtype_id,
				properties->\'title\' as title,
				properties->\'thumbnail\' as thumbnail
			'))
			->where('attribtype_id', '=', $attribtype->id)
			->get();
			$attribtype['attribs'] = $attributes;
			$data['attribtypes'][] = $attribtype;
		}

		return View::make('products/sidelights/create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = array(
			'title' => 'required',
			'shape_id' => 'required'
		);

		if (isset($input['attributes']['Color']))
		{
			foreach($input['attributes']['Color'] as $color) {
				$rules['color_image_' . $color] = 'required|image';
			}	
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{	
			$messages = $validator->messages()->toArray();
			foreach ($messages as $field => $message) {
				if (strpos($field, 'color_image_') === 0) {
					$validator->messages()->add('color_image', 'Please upload an image for every color selected.');
					break;
				}
			}
			Session::flash('oldAttributes', (isset($input['attributes']) ? array_flatten($input['attributes']) : array()));
		    return Redirect::back()->withErrors($validator)->withInput();
	    }

	    if (isset($input['attributes']['Color']))
		{
			foreach($input['attributes']['Color'] as $color)
			{	
				if (Input::hasFile('color_image_' . $color))
				{
					$dir = public_path().'/images/sidelights';
				    $file = Input::file('color_image_' . $color);
			    	$path = '/images/sidelights/' . $file->getBasename() . '.' . $file->getClientOriginalExtension();
			    	$file->move($dir, $path);
			    	$files[$color] = $path;
				}
			}
		}
		$hstoreFiles = PHPG_Utils::hstoreFromPhp($files);

		$sidelight = new Sidelight;
		$sidelight->title = $input['title'];
		$sidelight->shape_id = $input['shape_id'];
		$sidelight->color_image = $hstoreFiles;
		$sidelight->save();

		foreach(array_flatten($input['attributes']) as $attribute) {
			$sidelight->attributes()->attach($attribute, array('created_at' => 'now', 'updated_at' => 'now'));
		}

		return Redirect::action('SidelightController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}