<?php

class OverlayController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$url = URL::previous();
		$path = parse_url($url, PHP_URL_PATH);
		$segments = explode('/', trim($path, '/'));

		if ($segments[1] == 'sidelights') {
			return Redirect::action('OverlayController@indexSidelights');
		} else {
			return Redirect::action('OverlayController@indexDoors');
		}
		//$data['overlays'] = Overlay::all();
		//return View::make('products/' . $segments[1] . '/overlays/index')->with($data);
	}

	public function indexDoors()
	{
		$data['overlays'] = Overlay::all();
		return View::make('products/doors/overlays/index')->with($data);
	}

	public function indexSidelights()
	{
		$data['overlays'] = Overlay::all();
		return View::make('products/sidelights/overlays/index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$url = URL::previous();
		$path = parse_url($url, PHP_URL_PATH);
		$segments = explode('/', trim($path, '/'));

		if ($segments[1] == 'sidelights') {
			return Redirect::action('OverlayController@createSidelights');
		} else {
			return Redirect::action('OverlayController@createDoors');
		}
	}

	public function createDoors()
	{
		$glassAttribtype = Attribtype::where('title', '=', 'Glasstype')->first();
		$attributes = Attribute::select(DB::raw('
			id,
			attribtype_id,
			properties->\'title\' as title,
			properties->\'thumbnail\' as thumbnail
		'))
		->where('attribtype_id', '=', $glassAttribtype->id)
		->get();

		$data['attributes'] = $attributes;
		$data['shapes'] = Shape::where('type', '=', 'door')->get();

		return View::make('products/doors/overlays/create')->with($data);
	}

	public function createSidelights()
	{
		$glassAttribtype = Attribtype::where('title', '=', 'Glasstype')->first();
		$attributes = Attribute::select(DB::raw('
			id,
			attribtype_id,
			properties->\'title\' as title,
			properties->\'thumbnail\' as thumbnail
		'))
		->where('attribtype_id', '=', $glassAttribtype->id)
		->get();

		$data['attributes'] = $attributes;
		$data['shapes'] = Shape::where('type', '=', 'sidelight')->get();

		return View::make('products/sidelights/overlays/create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		//var_dump($input);die;
		$rules = array(
			'shape_id' => 'required',
			'attribute_id' => 'required',
			'image' => 'required|image'
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
		    return Redirect::back()->withInput()->withErrors($validator);
	    }

	    $dir = public_path().'/images/overlays';
	    $file = Input::file('image');
    	$path = '/images/overlays/' . $file->getBasename() . '.' . $file->getClientOriginalExtension();
    	$file->move($dir, $path);

	    $overlay = new Overlay;
		$overlay->shape_id = $input['shape_id'];
		$overlay->attribute_id = $input['attribute_id'];
		$overlay->image = $path;
		$overlay->save();

		// TODO: redirect to previous URL
		return Redirect::action('OverlayController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}