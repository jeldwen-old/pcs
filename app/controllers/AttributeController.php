<?php

class AttributeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$attributes = Attribute::select(DB::raw('
				id,
				attribtype_id,
				properties->\'title\' as title,
				properties->\'thumbnail\' as thumbnail
		'))->get();

		$data['attributes'] = $attributes;
		$data['attribtypes'] = Attribtype::all();
		return View::make('attributes/index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['attribtypes'] = Attribtype::all();
		return View::make('attributes/create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		//var_dump($input);die;
		$rules = array(
			'title' => 'required',
			'attribtype_id' => 'required',
			'thumbnail' => 'required|image'
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
		    return Redirect::back()->withInput()->withErrors($validator);
	    }

	    $dir = public_path().'/images/attributes';
	    $file = Input::file('thumbnail');
    	$path = '/images/attributes/' . $file->getBasename() . '.' . $file->getClientOriginalExtension();
    	$file->move($dir, $path);

	    $attribute = new Attribute;
		$attribute->attribtype_id = $input['attribtype_id'];
		$attribute->parent_id = 0;
		$attribute->properties = '
			title => "' . $input['title'] . '",
			thumbnail => "' . $path . '"
		';
		$attribute->save();

		return Redirect::action('AttributeController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}