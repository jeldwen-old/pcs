<?php

class DoorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['doors'] = Door::all();
		return View::make('products/doors/index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['configurations'] = Configuration::all();
		$data['sidelights'] = Sidelight::all();
		$data['shapes'] = Shape::where('type', '=', 'door')->get();

		$attribtypes = Attribtype::all();
		foreach ($attribtypes as $attribtype) {
			$attributes = Attribute::select(DB::raw('
				id,
				attribtype_id,
				properties->\'title\' as title,
				properties->\'thumbnail\' as thumbnail
			'))
			->where('attribtype_id', '=', $attribtype->id)
			->get();
			$attribtype['attribs'] = $attributes;
			$data['attribtypes'][] = $attribtype;
		}

		return View::make('products/doors/create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = array(
			'title' => 'required',
			'shape_id' => 'required',
			'sidelight_id' => 'required'
		);

		if (isset($input['attributes']['Color']))
		{
			foreach($input['attributes']['Color'] as $color) {
				$rules['color_image_' . $color] = 'required|image';
			}	
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{	
			$messages = $validator->messages()->toArray();
			foreach ($messages as $field => $message) {
				if (strpos($field, 'color_image_') === 0) {
					$validator->messages()->add('color_image', 'Please upload an image for every color selected.');
					break;
				}
			}
			Session::flash('oldConfigurations', (isset($input['configurations']) ? $input['configurations'] : array()));
			Session::flash('oldSidelights', (isset($input['sidelights']) ? $input['sidelights'] : array()));
			Session::flash('oldAttributes', (isset($input['attributes']) ? array_flatten($input['attributes']) : array()));
		    return Redirect::back()->withErrors($validator)->withInput();
	    }

	    $files = array();
	    if (isset($input['attributes']['Color']))
		{
			foreach($input['attributes']['Color'] as $color)
			{	
				if (Input::hasFile('color_image_' . $color))
				{
					$dir = public_path().'/images/doors';
				    $file = Input::file('color_image_' . $color);
			    	$path = '/images/doors/' . $file->getBasename() . '.' . $file->getClientOriginalExtension();
			    	$file->move($dir, $path);
			    	$files[$color] = $path;
				}
			}
		}
		$hstoreFiles = PHPG_Utils::hstoreFromPhp($files);

		$door = new Door;
		$door->title = $input['title'];
		$door->shape_id = $input['shape_id'];
		$door->color_image = $hstoreFiles;
		$door->save();

		$door->sidelights()->attach($input['sidelight_id'], array('created_at' => 'now', 'updated_at' => 'now'));

		if (isset($input['attributes'])) {
			foreach(array_flatten($input['attributes']) as $attribute) {
				$door->attributes()->attach($attribute, array('created_at' => 'now', 'updated_at' => 'now'));
			}
		}

		if (isset($input['configurations'])) {
			foreach(array_flatten($input['configurations']) as $configuration) {
				$door->configurations()->attach($configuration, array('created_at' => 'now', 'updated_at' => 'now'));
			}
		}

		return Redirect::action('DoorController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['door'] = Door::find($id);
		$data['configurations'] = Configuration::all();
		$data['sidelights'] = Sidelight::all();
		$data['shapes'] = Shape::where('type', '=', 'door')->get();

		$data['checkedConfigurations'] = $data['door']->configurations()->get()->lists('id');
		$data['checkedAttributes'] = $data['door']->attributes()->get()->lists('id');

		$attribtypes = Attribtype::all();
		foreach ($attribtypes as $attribtype) {
			$attributes = Attribute::select(DB::raw('
				id,
				attribtype_id,
				properties->\'title\' as title,
				properties->\'thumbnail\' as thumbnail
			'))
			->where('attribtype_id', '=', $attribtype->id)
			->get();
			$attribtype['attribs'] = $attributes;
			$data['attribtypes'][] = $attribtype;
		}

		return View::make('products/doors/edit')->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$rules = array(
			'title' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{	
			Session::flash('oldConfigurations', (isset($input['configurations']) ? $input['configurations'] : array()));
		    return Redirect::back()->withErrors($validator)->withInput();
	    }

		$door = Door::find($id);
		$door->title = $input['title'];
		$door->save();

		$door->configurations()->detach();


		if (isset($input['configurations'])) {
			foreach(array_flatten($input['configurations']) as $configuration) {
				$door->configurations()->attach($configuration, array('created_at' => 'now', 'updated_at' => 'now'));
			}
		}

		return Redirect::action('DoorController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}