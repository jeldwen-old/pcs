<?php

class Configuration extends Eloquent {

	public function doors()
    {
        return $this->belongsToMany('Door');
    }

}