<?php

class Overlay extends Eloquent {

	public function shapes()
    {
        return $this->belongsToMany('Shape');
    }

    public function attributes()
    {
        return $this->belongsToMany('Attribute');
    }

}