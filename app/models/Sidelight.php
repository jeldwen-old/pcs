<?php

class Sidelight extends Eloquent {

	public function attributes()
    {
        return $this->belongsToMany('Attribute');
    }

    public function doors()
    {
        return $this->belongsToMany('Door');
    }

    public function shapes()
    {
        return $this->morphMany('Shape', 'imageable');
    }

}