<?php

class Shape extends Eloquent {

	public function imageable()
    {
        return $this->morphTo();
    }

    public function overlays()
    {
        return $this->belongsToMany('Overlay');
    }

}