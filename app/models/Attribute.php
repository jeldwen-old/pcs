<?php

class Attribute extends Eloquent {

	public function attribtype()
    {
        return $this->belongsTo('Attribtype');
    }

    public function doors()
    {
        return $this->belongsToMany('Door');
    }

    public function sidelights()
    {
        return $this->belongsToMany('Sidelight');
    }


    public function products()
    {
        return $this->belongsToMany('Products');
    }

    // Will also belong to categories, lines, styles, etc. to be super inheritoverideable

    public function overlays()
    {
        return $this->belongsToMany('Overlay');
    }

}