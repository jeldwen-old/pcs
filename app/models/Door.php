<?php

class Door extends Eloquent {

	public function attributes()
    {
        return $this->belongsToMany('Attribute');
    }

    public function sidelights()
    {
        return $this->belongsToMany('Sidelight');
    }

    public function configurations()
    {
        return $this->belongsToMany('Configuration');
    }

    public function shapes()
    {
        return $this->morphMany('Shape', 'imageable');
    }

}