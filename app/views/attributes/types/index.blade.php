@extends('layouts/default')

@section('top')
	@include('menus/attributes/top')
@endsection

@section('content')

	<div class="large-12 columns">
	
		<table>
			<thead>
				<tr>
					<th width="100">ID</th>
					<th>Title</th>
				</tr>
			</thead>
			<tbody>
				@foreach($attribtypes as $attribtype)
					<tr>
						<td>{{ $attribtype->id }}</td>
						<td>{{ $attribtype->title }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<div class="panel">
			<small>TODO: Add CRUD to modify attribute type form fields, which will dynamically update the forms for attributes of this type.</small>
		</div>

	</div>

@endsection