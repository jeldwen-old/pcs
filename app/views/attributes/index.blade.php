@extends('layouts/default')

@section('top')
	@include('menus/attributes/top')
@endsection

@section('content')
	
	<div class="large-12 columns">

		<div class="row">
			<div class="large-12 columns">
				<a href="{{ URL::action('AttributeController@create') }}" class="small button success">New Attribute</a>
			</div>
		</div>
	
		<div class="row">
			<div class="large-12 columns">
				<table class="large-12 columns">
					<thead>
						<tr>
							<th width="100">ID</th>
							<th>Title</th>
							<th width="200">Type</th>
							<th width="50">Thumbnail</th>
						</tr>
					</thead>
					<tbody>
						@foreach($attributes as $attribute)
							<tr>
								<td>{{ $attribute->id }}</td>
								<td>{{ $attribute->title }}</td>
								<td>{{ $attribute->attribtype->title }}</td>
								<td class="thumbnail"><img src="{{ $attribute->thumbnail }}"></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>

@endsection