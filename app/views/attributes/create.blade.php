@extends('layouts/default')

@section('top')
	@include('menus/attributes/top')
@endsection

@section('content')
	
	<form action="{{ URL::action('AttributeController@store') }}" method="POST" enctype="multipart/form-data">

		<div class="large-12 columns">

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('AttributeController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_saveclose" value="Save & Close">
				</div>
			</div>

			<table class="large-12 columns">
				<tbody>
					<tr>
						<td class="field">Title</td>
						<td>
							{{ $errors->first('title', '<label class="error">:message</label>') }}
							<input type="text" name="title" placeholder="Title" value="{{ Input::old('title') }}" class="{{ (!$errors->first('title') ?: 'error') }}">
						</td>
					</tr>
					<tr>
						<td class="field">Attribute Type</td>
						<td>
							{{ $errors->first('attribtype_id', '<label class="error">:message</label>') }}
							<select name="attribtype_id">
								@foreach($attribtypes as $attribtype)
									<option value="{{ $attribtype->id }}">{{ $attribtype->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">Thumbnail</td>
						<td>
							{{ $errors->first('thumbnail', '<label class="error">:message</label>') }}
							<input type="file" name="thumbnail">
						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('AttributeController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_close" value="Save & Close">
				</div>
			</div>

		</div>

	</form>

@endsection