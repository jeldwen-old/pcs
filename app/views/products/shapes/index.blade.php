@extends('layouts/default')

@section('top')
	@include('menus/products/top')
@endsection

@section('content')
	
	<div class="large-12 columns">

		<div class="row">
			<div class="large-12 columns">
				<a href="#" class="small button success">New Shape</a>
			</div>
		</div>
	
		<div class="row">
			<div class="large-12 columns">
				<table class="large-12 columns">
					<thead>
						<tr>
							<th width="100">ID</th>
							<th>Title</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
						@foreach($shapes as $shape)
							<tr>
								<td>{{ $shape->id }}</td>
								<td>{{ $shape->title }}</td>
								<td>{{ $shape->type }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>

@endsection