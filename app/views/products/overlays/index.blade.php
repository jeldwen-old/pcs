@extends('layouts/default')

@section('top')
	@include('menus/products/top')
@endsection

@section('content')
	
	<div class="large-12 columns">

		<div class="row">
			<div class="large-12 columns">
				<a href="{{ URL::action('OverlayController@create') }}" class="small button success">New Overlay</a>
			</div>
		</div>
	
		<div class="row">
			<div class="large-12 columns">
				<table class="large-12 columns">
					<thead>
						<tr>
							<th width="100">ID</th>
							<th>Title</th>
							<th>Shape</th>
							<th>Attribute</th>
							<th>Image</th>
						</tr>
					</thead>
					<tbody>
						@foreach($overlays as $overlay)
							<tr>
								<td>{{ $overlay->id }}</td>
								<td>{{ $overlay->title }}</td>
								<td>{{ $overlay->type }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>

@endsection