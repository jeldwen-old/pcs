@extends('layouts/default')

@section('top')
	@include('menus/products/top')
@endsection

@section('content')
	
	<form id="create-sidelight" action="{{ URL::action('OverlayController@store') }}" method="POST" enctype="multipart/form-data">

		<div class="large-12 columns">

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('SidelightController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_saveclose" value="Save & Close">
				</div>
			</div>

			<table class="large-12 columns">
				<tbody>
					<tr>
						<td class="field" width="200">
							Title
						</td>
						<td>
							{{ $errors->first('title', '<label class="error">:message</label>') }}
							<input type="text" name="title" placeholder="Title" value="{{ Input::old('title') }}" class="{{ (!$errors->first('title') ?: 'error') }}">
						</td>
					</tr>
					<tr>
						<td class="field">Shape</td>
						<td>
							{{ $errors->first('shape_id', '<label class="error">:message</label>') }}
							<select name="attribtype_id">
								@foreach($shapes as $shape)
									<option value="{{ $shape->id }}">{{ $shape->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">Glasstype</td>
						<td>
							{{ $errors->first('attribute_id', '<label class="error">:message</label>') }}
							<select name="attribtype_id">
								@foreach($attributes as $attribute)
									<option value="{{ $attribute->id }}">{{ $attribute->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('SidelightController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_close" value="Save & Close">
				</div>
			</div>

		</div>

	</form>

@endsection