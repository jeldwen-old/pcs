@extends('layouts/default')

@section('top')
	<div class="large-12 columns">
		<dl class="sub-nav">
			<dd class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></dd>
			<dd><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></dd>
			<dd><a href="{{ URL::action('ShapeController@index') }}">Shapes</a></dd>
		</dl>
	</div>
@endsection

@section('left')
	<div class="large-3 columns">
		<ul class="side-nav">
			<li><a href="{{ URL::action('DoorController@index') }}">Doors</a></li>
			<li class="divider"></li>
			<li class="active"><a href="{{ URL::action('OverlayController@index') }}">Overlays</a></li>
		</ul>
	</div>
@endsection

@section('content')
	
	<form id="create-sidelight" action="{{ URL::action('OverlayController@store') }}" method="POST" enctype="multipart/form-data">

		<div class="large-9 columns">

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('OverlayController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_saveclose" value="Save & Close">
				</div>
			</div>

			<div class="row">
				<div class="large-12 columns">
					<p>You are uploading a <strong>door</strong> overlay!</p>
				</div>
			</div>

			<table class="large-12 columns">
				<tbody>
					<tr>
						<td class="field">Shape</td>
						<td>
							{{ $errors->first('shape_id', '<label class="error">:message</label>') }}
							<select name="shape_id">
								@foreach($shapes as $shape)
									<option value="{{ $shape->id }}">{{ $shape->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">Glasstype</td>
						<td>
							{{ $errors->first('attribute_id', '<label class="error">:message</label>') }}
							<select name="attribute_id">
								@foreach($attributes as $attribute)
									<option value="{{ $attribute->id }}">{{ $attribute->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">
							Image
						</td>
						<td>
							{{ $errors->first('image', '<label class="error">:message</label>') }}
							<input type="file" name="image">
						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('SidelightController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_close" value="Save & Close">
				</div>
			</div>

		</div>

	</form>

@endsection