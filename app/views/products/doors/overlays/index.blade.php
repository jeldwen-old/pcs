@extends('layouts/default')

@section('top')
	<div class="large-12 columns">
		<dl class="sub-nav">
			<dd class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></dd>
			<dd><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></dd>
			<dd><a href="{{ URL::action('ShapeController@index') }}">Shapes</a></dd>
		</dl>
	</div>
@endsection

@section('left')
	<div class="large-3 columns">
		<ul class="side-nav">
			<li><a href="{{ URL::action('DoorController@index') }}">Doors</a></li>
			<li class="divider"></li>
			<li class="active"><a href="{{ URL::action('OverlayController@index') }}">Overlays</a></li>
		</ul>
	</div>
@endsection

@section('content')
	
	<div class="large-9 columns">

		<div class="row">
			<div class="large-12 columns">
				<a href="{{ URL::action('OverlayController@create') }}" class="small button success">New Door Overlay</a>
			</div>
		</div>
	
		<div class="row">
			<div class="large-12 columns">
				<table class="large-12 columns">
					<thead>
						<tr>
							<th width="100">ID</th>
							<th>Shape</th>
							<th>Attribute</th>
							<th width="50">Image</th>
						</tr>
					</thead>
					<tbody>
						@foreach($overlays as $overlay)
							<tr>
								<td>{{ $overlay->id }}</td>
								<td>{{ $overlay->shape_id }}</td>
								<td>{{ $overlay->attribute_id }}</td>
								<td class="thumbnail"><img src="{{ $overlay->image }}"></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>

@endsection