@extends('layouts/default')

@section('top')
	<div class="large-12 columns">
		<dl class="sub-nav">
			<dd class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></dd>
			<dd><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></dd>
			<dd><a href="{{ URL::action('ShapeController@index') }}">Shapes</a></dd>
		</dl>
	</div>
@endsection

@section('left')
	<div class="large-3 columns">
		<ul class="side-nav">
			<li class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></li>
			<li class="divider"></li>
			<li><a href="{{ URL::action('OverlayController@index') }}">Overlays</a></li>
		</ul>
	</div>
@endsection

@section('content')
	
	<form id="create-door" action="{{ URL::action('DoorController@update', $door->id) }}" method="POST" enctype="multipart/form-data">

		<div class="large-9 columns">

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('DoorController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_saveclose" value="Save & Close">
				</div>
			</div>

			<table class="large-12 columns">
				<tbody>
					<tr>
						<td class="field" width="200">
							Title
						</td>
						<td>
							{{ $errors->first('title', '<label class="error">:message</label>') }}
							<input type="text" name="title" placeholder="Title" value="{{ Input::old('title', $door->title) }}" class="{{ (!$errors->first('title') ?: 'error') }}">
						</td>
					</tr>
					<tr>
						<td class="field">
							Configurations
						</td>
						<td>
							@foreach($configurations as $configuration)
								<label>
									<input type="checkbox" name="configurations[]" value="{{ $configuration->id }}"
										{{ in_array($configuration->id, $checkedConfigurations) ? 'checked' : '' }}
										@if (is_array(Session::get('oldConfigurations')))
											{{ (in_array($configuration->id, Session::get('oldConfigurations')) ? 'checked' : '') }}
										@endif
									>
									{{ $configuration->title }}
								</label>
							@endforeach
						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('AttributeController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_close" value="Save & Close">
				</div>
			</div>

		</div>

		<input type="hidden" name="_method" value="PUT" />
	</form>

@endsection