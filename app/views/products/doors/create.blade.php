@extends('layouts/default')

@section('top')
	<div class="large-12 columns">
		<dl class="sub-nav">
			<dd class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></dd>
			<dd><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></dd>
			<dd><a href="{{ URL::action('ShapeController@index') }}">Shapes</a></dd>
		</dl>
	</div>
@endsection

@section('left')
	<div class="large-3 columns">
		<ul class="side-nav">
			<li class="active"><a href="{{ URL::action('DoorController@index') }}">Doors</a></li>
			<li class="divider"></li>
			<li><a href="{{ URL::action('OverlayController@index') }}">Overlays</a></li>
		</ul>
	</div>
@endsection

@section('content')
	
	<form id="create-door" action="{{ URL::action('DoorController@store') }}" method="POST" enctype="multipart/form-data">

		<div class="large-9 columns">

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('DoorController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_saveclose" value="Save & Close">
				</div>
			</div>

			<table class="large-12 columns">
				<tbody>
					<tr>
						<td class="field" width="200">
							Title
						</td>
						<td>
							{{ $errors->first('title', '<label class="error">:message</label>') }}
							<input type="text" name="title" placeholder="Title" value="{{ Input::old('title') }}" class="{{ (!$errors->first('title') ?: 'error') }}">
						</td>
					</tr>
					<tr>
						<td class="field">Shape</td>
						<td>
							{{ $errors->first('shape_id', '<label class="error">:message</label>') }}
							<select name="shape_id">
								@foreach($shapes as $shape)
									<option value="{{ $shape->id }}">{{ $shape->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">Sidelight</td>
						<td>
							{{ $errors->first('sidelight_id', '<label class="error">:message</label>') }}
							<select name="sidelight_id">
								@foreach($sidelights as $sidelight)
									<option value="{{ $sidelight->id }}">{{ $sidelight->title }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">
							Configurations
						</td>
						<td>
							@foreach($configurations as $configuration)
								<label>
									<input type="checkbox" name="configurations[]" value="{{ $configuration->id }}"
										@if (is_array(Session::get('oldConfigurations')))
											{{ (in_array($configuration->id, Session::get('oldConfigurations')) ? 'checked' : '') }}
										@endif
									>
									{{ $configuration->title }}
								</label>
							@endforeach
						</td>
					</tr>
					<tr>
						<td class="field">
							Attributes
						</td>
						<td>
							{{ $errors->first('color_image', '<label class="error">:message</label>') }}
							@foreach($attribtypes as $attribtype)
								<fieldset>
									<legend>{{ $attribtype->title }}</legend>
									<table id="attributes-{{ $attribtype->title }}">
										<tbody>
											@foreach($attribtype->attribs as $attribute)
												<tr class="checkbox">
													<td>
														<input type="checkbox" name="attributes[{{ $attribtype->title }}][]" value="{{ $attribute->id }}"
															@if (is_array(Session::get('oldConfigurations')))
																{{ (in_array($attribute->id, Session::get('oldAttributes')) ? 'checked' : '') }}
															@endif
														>
													</td>
													<td>{{ $attribute->title }}</td>
													<td class="thumbnail"><img src="{{ $attribute->thumbnail }}"></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</fieldset>
							@endforeach

						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="large-12 columns">
					<a href="{{ URL::action('AttributeController@index') }}" class="button small alert">Cancel</a>
					<input type="submit" class="button small" name="submitbtn_save" value="Save">
					<input type="submit" class="button small success" name="submitbtn_close" value="Save & Close">
				</div>
			</div>

		</div>

	</form>

@endsection