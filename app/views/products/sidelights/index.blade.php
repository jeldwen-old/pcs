@extends('layouts/default')

@section('top')
	<div class="large-12 columns">
		<dl class="sub-nav">
			<dd><a href="{{ URL::action('DoorController@index') }}">Doors</a></dd>
			<dd class="active"><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></dd>
			<dd><a href="{{ URL::action('ShapeController@index') }}">Shapes</a></dd>
		</dl>
	</div>
@endsection

@section('left')
	<div class="large-3 columns">
		<ul class="side-nav">
			<li class="active"><a href="{{ URL::action('SidelightController@index') }}">Sidelights</a></li>
			<li class="divider"></li>
			<li><a href="{{ URL::action('OverlayController@index') }}">Overlays</a></li>
		</ul>
	</div>
@endsection

@section('content')
	
	<div class="large-9 columns">

		<div class="row">
			<div class="large-12 columns">
				<a href="{{ URL::action('SidelightController@create') }}" class="small button success">New Sidelight</a>
			</div>
		</div>
	
		<div class="row">
			<div class="large-12 columns">
				<table class="large-12 columns">
					<thead>
						<tr>
							<th width="100">ID</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						@foreach($sidelights as $sidelight)
							<tr>
								<td>{{ $sidelight->id }}</td>
								<td>{{ $sidelight->title }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>

@endsection