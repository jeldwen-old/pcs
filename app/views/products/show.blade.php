@extends('layouts/frontend')

@section('content')
	
	<div class="large-12 columns">

		<div class="breadcrumbs">
			<a href="{{ URL::action('ProductController@index') }}">Go Back</a>
		</div>

		<h4>{{ $product->title }}</h4>

		<div id="pdp" class="row">

			<div class="large-8 columns">

				<img id="product_image" src="{{ $product->defaultimage }}">

			</div>

			<div class="large-4 columns">

				<div class="panel">
					<h6>Product Description</h6>
					<hr>
					...
				</div>

				<div id="colors" class="panel">
					<h6>Colors</h6>
					<hr>
					<div class="row">
						@foreach($colorAttributes as $attribute)
							<div class="large-3 columns left">
								<img class="has-tip tip-left thumb" data-id="{{ $attribute->id }}" data-tooltip title="{{ $attribute->title }}" src="{{ $attribute->thumbnail }}">
							</div>
						@endforeach
					</div>
				</div>

				<div id="glasstypes" class="panel">
					<h6>Glasstypes</h6>
					<hr>
					<div class="row">
						@foreach($glassAttributes as $attribute)
							<div class="large-3 columns left">
								<img class="has-tip tip-left thumb" data-id="{{ $attribute->id }}" data-tooltip title="{{ $attribute->title }}" src="{{ $attribute->thumbnail }}">
							</div>
						@endforeach
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection