@extends('layouts/frontend')

@section('content')
	
	<div class="large-12 columns">

		<div class="row">
			@foreach($products as $product)
				<div class="large-4 columns">
					<a href="{{ URL::action('ProductController@show', $product->id) }}">
						<div class="panel product">
							<img src="{{ $product->defaultimage }}">
							<h5>{{ $product->title }}</h5>
						</div>
					</a>
				</div>
			@endforeach
		</div>

	</div>

@endsection