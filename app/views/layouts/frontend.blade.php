<html>

	@include('partials/head')

	<body>

		@include('menus/frontend')

		<div class="row">

			@yield('top')

		</div>
		
		<div class="row">

			@yield('left')

			@yield('content')

			@yield('right')

		</div>

		@include('partials/foot')

	</body>

</html>