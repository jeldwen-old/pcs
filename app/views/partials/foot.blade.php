{{ Asset::queue('jquery', 'js/vendor/jquery.js') }}
{{ Asset::queue('foundation', 'js/foundation.min.js', 'jquery') }}
{{ Asset::queue('default', 'js/global.js', 'foundation') }}

@foreach (Asset::getCompiledScripts() as $script)
	<script src="{{ $script }}"></script>
@endforeach

<script>$(document).foundation();</script>