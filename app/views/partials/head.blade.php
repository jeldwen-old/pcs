<head>

	<title>JELD-WEN Product Configuration System</title>

	{{ Asset::queue('normalize', 'css/normalize.css') }}
	{{ Asset::queue('foundation', 'css/foundation.min.css', 'normalize') }}
	{{ Asset::queue('default', 'css/default.css', 'foundation') }}

	@foreach (Asset::getCompiledStyles() as $style)
		<link href="{{ $style }}" rel="stylesheet">
	@endforeach

	<script src="{{ Asset::getUrl('js/vendor/custom.modernizr.js') }}"></script>

</head>