<nav class="top-bar" style="">
	<ul class="title-area">
		<li class="name">
			<h1><a href="/">JELD-WEN</a></h1>
		</li>
		<li class="toggle-topbar menu-icon">
			<a href="#"><span>Menu</span></a>
		</li>
	</ul>
	<section class="top-bar-section">
		<ul class="left">
			<!--<li><a href="#">Categories</a></li>-->
			<li class="divider"></li>
			<li><a href="{{ URL::action('DoorController@index') }}">Products</a></li>
			<li class="divider"></li>
			<li><a href="{{ URL::action('AttributeController@index') }}">Attributes</a></li>
			<li class="divider"></li>
		</ul>
	</section>
	<section class="top-bar-section">
		<ul class="right">
			<li><a href="{{ URL::action('ProductController@clearDatabase') }}" class="button alert">Clear Database</a></li>
			<li><a href="{{ URL::action('ProductController@process') }}" class="button success">Process Products</a></li>
			<li class="divider"></li>
			<li><a href="{{ URL::action('ProductController@index') }}">Frontend</a></li>
		</ul>
	</section>
</nav>