<nav class="top-bar" style="">
	<ul class="title-area">
		<li class="name">
			<h1><a href="/">JELD-WEN</a></h1>
		</li>
		<li class="toggle-topbar menu-icon">
			<a href="#"><span>Menu</span></a>
		</li>
	</ul>
	<section class="top-bar-section">
		<ul class="left">
		</ul>
	</section>
	<section class="top-bar-section">
		<ul class="right">
			<li class="divider"></li>
			<li><a href="{{ URL::action('DoorController@index') }}">Backend</a></li>
		</ul>
	</section>
</nav>