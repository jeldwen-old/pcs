<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('products');
});

Route::resource('attributes/types', 'AttribtypeController');
Route::resource('attributes', 'AttributeController');

Route::get('products/doors/overlays/create', 'OverlayController@createDoors');
Route::get('products/doors/overlays', 'OverlayController@indexDoors');
Route::resource('products/doors', 'DoorController');

Route::get('products/sidelights/overlays/create', 'OverlayController@createSidelights');
Route::get('products/sidelights/overlays', 'OverlayController@indexSidelights');
Route::resource('products/sidelights', 'SidelightController');

Route::resource('products/overlays', 'OverlayController');
Route::resource('products/shapes', 'ShapeController');

Route::get('products/process', 'ProductController@process');
Route::resource('products', 'ProductController');

Route::get('cleardb', 'ProductController@clearDatabase');