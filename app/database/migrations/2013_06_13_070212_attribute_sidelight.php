<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttributeSidelight extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attribute_sidelight', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('attribute_id');
			$table->integer('sidelight_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attribute_sidelight');
	}

}
