<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('
			CREATE TABLE "attributes" (
				"id" serial PRIMARY KEY,
				"created_at" timestamp NOT NULL,
				"updated_at" timestamp NOT NULL,
				"attribtype_id" integer NOT NULL,
				"parent_id" integer NOT NULL,
				"properties" hstore
			);
		');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attributes');
	}

}
