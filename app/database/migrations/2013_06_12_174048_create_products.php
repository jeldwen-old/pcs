<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('
			CREATE TABLE "products" (
				"id" serial PRIMARY KEY,
				"created_at" timestamp NOT NULL,
				"updated_at" timestamp NOT NULL,
				"title" varchar NOT NULL,
				"images" hstore,
				"attributes" hstore
			)
		');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
