<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('
			CREATE TABLE "doors" (
				"id" serial PRIMARY KEY,
				"created_at" timestamp,
				"updated_at" timestamp,
				"title" varchar NOT NULL,
				"shape_id" integer NOT NULL,
				"color_image" hstore
			);
		');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('doors');
	}

}
