<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoorSidelight extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('door_sidelight', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('door_id');
			$table->integer('sidelight_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('door_sidelight');
	}

}
