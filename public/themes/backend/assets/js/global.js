$(document).ready(function(){

	$('form tr.checkbox td').not('.file').click(function(){
		$(this).closest('tr').find('input[type="checkbox"]').trigger('click');
	});

	$('table#attributes-Color input[type="checkbox"]').on('change', function(){
		if(this.checked) {
			$(this).closest('tr').append('<td class="file"><input type="file" name="color_image_' + $(this).val() + '"></td>');
		} else {
			$(this).closest('tr').find('.file').remove();
		}
	});

	if($('#pdp').length > 0) {
		var pdpImgHref = $('#pdp #product_image').attr('src');
		var pdpImgFile = pdpImgHref.replace(/^.*[\\\/]/, '').split('.');
		var pdpImgSegments = pdpImgFile[0].split('_');

		$('#pdp #colors').find('img').on('click', function(){
			pdpImgSegments[1] = $(this).data('id');
			$('#pdp #product_image').attr('src', '/images/products/' + pdpImgSegments.join('_') + '.jpg');
		});

		$('#pdp #glasstypes').find('img').on('click', function(){
			pdpImgSegments[2] = $(this).data('id');
			$('#pdp #product_image').attr('src', '/images/products/' + pdpImgSegments.join('_') + '.jpg');
		});
	}

});

